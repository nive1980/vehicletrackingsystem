create table vehicle_make(id int IDENTITY(1,1),name varchar(50) default null,PRIMARY KEY (id));
create table vehicle_model(id int IDENTITY(1,1),name varchar(50) default null,make int foreign key references vehicle_make(id) not null,PRIMARY KEY (id));
create table vehicle (id int IDENTITY(1,1),name varchar(50) default null,number varchar(50), model int references vehicle_model(id));
create table distance(id int IDENTITY(1,1),location1 varchar(50),location2 varchar(50),distance float(50),time float(50),PRIMARY KEY (id));
SET IDENTITY_INSERT distance ON

insert into distance values(1,1,3,1700,27);
insert into distance values(2,1,2,550,8);
insert into distance values(3,3,2,2200,34);

SET IDENTITY_INSERT vehicle ON
GO

insert into vehicle (ID,NAME) values(1,'Tata');
insert into vehicle (ID,NAME) values(2,'Suzuki');
insert into vehicle (ID,NAME) values(3,'Honda');
insert into vehicle (ID,NAME) values(4,'Merc Benz');
SET IDENTITY_INSERT vehicle ON
insert into vehicle_MODEL (ID,NAME,MAKE) values(1,'Nano',1);
insert into vehicle_MODEL (ID,NAME,MAKE) values(2,'Prima',1);
insert into vehicle_MODEL (ID,NAME,MAKE) values(3,'Starbus',1);

insert into vehicle_MODEL (ID,NAME,MAKE) values(4,'Zen',2);
insert into vehicle_MODEL (ID,NAME,MAKE) values(5,'Vitara',2);
insert into vehicle_MODEL (ID,NAME,MAKE) values(6,'Ritz',2);


insert into vehicle_MODEL (ID,NAME,MAKE) values(7,'Civic',3);
insert into vehicle_MODEL (ID,NAME,MAKE) values(8,'C200',4);
insert into vehicle_MODEL (ID,NAME,MAKE) values(9,'GLA120',4);

create table location(id int identity(1,1),name varchar(50));
insert into location(id,name) vaues(1,'Hyderabad Airport');

insert into location(id,name) vaues(2,'Bangalore Airport');
insert into location(id,name) vaues(3,'Delhi Airport');

create table booking(id int IDENTITY(1,1),user_id int,pick_up_location int ,drop_off_location int ,vehicle int ,pick_up_Date datetime,drop_off_date datetime);
SET IDENTITY_INSERT publication off 
GO
INSERT INTO PUBLICATION( TITLE ,
  AUTHOR,
  PUBLISH_DATE,
  PUBLICATION_TYPE,
  DESCRIPTION,
  PUB_URL,
  PRICE) VALUES ('Paper.pdf','Nivedita','2012-12-12 00:00:00','paper','UK Investment Fund','http://localhost:8080/gtn/pdf/Paper.pdf',500.00
);
INSERT INTO PUBLICATION VALUES ('Book.pdf','Nivedita','2012-12-12 00:00:00','Book','My Book','http://localhost:8080/gtn/pdf/Book.pdf',400.00
);

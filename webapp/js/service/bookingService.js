restControllers.service('bookingService',function($http,$cookies){
	this.getBookingsToDisplay = function(){
		return $http({
			method : 'POST',
			url : '/vehicleTracking/getBookingsToDisplay',
			data : {
				"userId" : $cookies.get('username')								
			},
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Basic ' + btoa($cookies.get('username') + ':' + $cookies.get('password'))
			}
		})
	};
	
	this.fetchMakeToModelMapping = function(){
			return $http({
				method : 'POST',
				url : '/vehicleTracking/fetchMakeToModelMapping',
				data : {
					"userId" : $cookies.get('username')								
				},
				headers : {
					'Content-Type' : 'application/json',
					'Authorization' : 'Basic ' + btoa($cookies.get('username') + ':' + $cookies.get('password'))
				}
			})
	};
	
	this.createBooking= function(userId,pickUpLocation,dropoffLocation,make,model,pickUpTime,dropoffTime) {
	
		return $http({
			method : 'POST',
			url : '/vehicleTracking/saveBooking',
			data : {
				"pickupLocation" : pickUpLocation,
				"dropoffLocation" : dropoffLocation,
				"vehicleMake" : make,
				"vehicleModel" : model,
				"pickupDate" : pickUpTime,
				"dropoffDate":dropoffTime,
				"userId":userId
			},
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Basic ' + btoa($cookies.get('username') + ':' + $cookies.get('password'))
			}
		})
	};
	this.getActivityData = function(){
		return $http({
			method : 'POST',
			url : '/vehicleTracking/getActivityDataForCase',
			data : {
				"userId" : $cookies.get('username'),
			},
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Basic ' + btoa($cookies.get('username') + ':' + $cookies.get('password'))
			}
		})
	}
	
});
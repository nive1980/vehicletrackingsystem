dashboardApp.service('publicationService',function($http,$cookies){
	this.arePublicationsPresent = function() {													
	return	$http(
				{
					method : 'POST',
					responseType:'json',
					url : '/gtn/getPublications',
					data : {
							"email" : $cookies.get('username')
					},
					headers : {
							'Content-Type' : 'application/json',
							'Authorization' : 'Basic ' + btoa($cookies.get('username') + ':' + $cookies.get('password'))
					}								
       })

       };	
 this.getPublicationCartInfo = function(){
       		return $http({
       		    url: "getPublicationCartInfo",
       		    responseType:"json",
       		    method: "GET",
       		    headers: {
       		        "Content-Type": "application/json"
       		    }
       		});
       	}
	
});
screeningController.controller('entityScreeningCtlr', ['$scope', '$location', '$http', '$compile', 'entityScreeningFactory', '$sce', 'commonService',
    function($scope, $location, $http, $compile, entityScreeningFactory, $sce, commonService) {
		
		$scope.entities = [];
		
		/*$scope.errors = [];
		$scope.warnings = [];
		$scope.infos = [];*/
	
		$scope.init = function(){
			/*$scope.$on('$routeChangeSuccess', function() {
				$('.selectpicker').selectpicker();
				$scope.reset();
			});*/
			$scope.reset();
			/*$scope.entities.push({
					entityType:'Carrier', orgType:'Individual', entityId:'12345', city:'Mumbai', name:'Naveen', 
						state:'Maharashtra', addressLine1:'Poonam nagar', addressLine2:'Jogeshwari east', addressLine3:'400096', 
						country:'IN', zip:'400096', edit: $sce.trustAsHtml("<a href='javascript:void(0)'>Edit</a>"),
						remove: $sce.trustAsHtml("<a href='javascript:void(0)' class='delete-entity'>Delete</a>")
			});
			
			var tbody = angular.element('#entity-table tbody');
			
			$compile(tbody)($scope);*/
		}
	
		$scope.addEntity = function(){
			
			//var tr = this.prepareEntityRow($scope.entity);
			
			//$('#entity-table tbody').append(tr);
			$scope.clearMessages();
			
			//$scope.entity.$index = $scope.entities.length;
			
			//$scope.entity.entityType = $scope.entityTypes[$scope.entity.entityType];
			
			/*var $el = "<a ng-click='deleteEntity("+$scope.entity.$index+")'>Delete</a>";
			$scope.entity.edit = $sce.trustAsHtml("<a ng-click='editEntity("+$scope.entity.$index+")'>Edit</a>");
			$scope.entity.remove = $sce.trustAsHtml($el);*/
			
			console.log($scope.entity);
			
			//alert($scope.entity.name);
			
			if($scope.entity.name!=''){
				if ($scope.entity.$index === undefined) {
					$scope.entities.push($scope.entity);
					$scope.entity.$index = $scope.entities.length;
				}else{
					//$scope.reset();
					//$scope.entities[$scope.entity.$index] = $scope.entity;
				}
				$scope.reset();
			}else{
				$scope.errors.push({form:'', field: '', msg: 'Please enter entity name.'});
				commonService.processMsg($scope.errors, 'error');
			}
			
			
			/*var tbody = angular.element('#entity-table tbody');
			
			$compile(tbody)($scope);*/
		}
		
		$scope.searchWlsEntity = function(){
			$scope.clearMessages();
			
			if($scope.entity.entityType == ''){
				$scope.errors.push({form:'', field: '', msg: 'Please select a entity type.'});
				return;
			}
			
			if($scope.entity.entityType == 'Exporter'){
				//$scope.searchExporterPopup();
				commonService.searchExporterPopup('wls', $scope.entity);
			}else if($scope.entity.entityType == 'Consignee'){
				//$scope.searchConsigneePopup('ultCons');
				commonService.searchConsigneePopup('wls', $scope.entity);
			}else{
				$scope.errors.push({form:'', field: '', msg: 'Master not found.'});
				return;
			}
		}
		
		/*$scope.prepareEntityRow = function(entityObj){
	    	
	    	var rowNum = 1;
	    	
	    	if($('#entity-table tbody tr').length > 0){
	    		rowNum = $('#entity-table tbody tr').length + 1;
	    	}
	    	    	
	    	//var selectedEntity = document.getElementById('entity_sl').options[document.getElementById('entity_sl').selectedIndex].text;
	    	
	    	var selectedEntity;
	    	
	    	if(entityObj.entityType == ''){
	    		selectedEntity = '';
	    	}else{
	    		selectedEntity = $scope.entityType[entityObj.entityType];
	    	}
	    		
	    	var tr = "<tr class='cart_item'>";
	  	    	    	
	    	tr += "<td>"+rowNum+"</td>";
	    	tr += "<td>"+selectedEntity+"</td>";
	    	tr += "<td>"+entityObj.orgType+"</td>";    	
	    	tr += "<td>"+entityObj.entityId+"</td>";
	    	tr += "<td>"+entityObj.name+"</td>";
	    	tr += "<td>"+entityObj.addressLine1+"</td>";
	    	tr += "<td>"+entityObj.city+"</td>";
	    	tr += "<td>"+entityObj.state+"</td>";
	    	tr += "<td>"+entityObj.zip+"</td>";
	    	tr += "<td><a href='javascript:void(0)'>Edit</a></td>";
	    	tr += "<td><a href='javascript:void(0)' class='delete-entity'>Delete</a><input type='text' value='"+JSON.stringify(entityObj)+"' class='hidden entity' /></td>";
	    	
	    	tr += "</tr>";
	    	
	    	return tr;
	    }*/
		
		/*$scope.entityTypes = {
				"": "",
				"CON": "Consignee",
				"FF": "Freight Forwarder",
				"C": "Carrier",
				"E": "Exporter"
		}*/
	
		$scope.reset = function(){
			$scope.entity = {
					entityType:'', orgType:'', entityId:'', city:'', name:'', state:'', addressLine1:'', addressLine2:'', addressLine3:'', country:'', zip:'' , entityCode: ''
			}
			//$('.selectpicker').selectpicker('refresh');
			$scope.clearMessages();
		}
		
		/**
		 * call factory method for screening
		 */
		$scope.screenEntity = function(){
			console.log($scope.entities);
			$scope.clearMessages();
			
			if($scope.entities.length == 0){
				$scope.errors.push({form:'', field: '', msg: 'Please enter at least one entity to screen.'});
				commonService.processMsg($scope.errors, 'error');
			}else{
				var promise = entityScreeningFactory.doWatchListScreening($scope.entities);
				
				promise.then(function(response){
					
					if(response.status == 401){
						$scope.errors.push({form:'', field: '', msg: 'Session expired'});
						commonService.processMsg($scope.errors, 'error');
						return;
					}else if(response.status == -1){
						$scope.errors.push({form:'', field: '', msg: 'Server encountered an error. Please try later.'});
						commonService.processMsg($scope.errors, 'error');
						return;
					}
					
					if(response.data.success == false){
						$scope.errors.push({form:'', field: '', msg: response.data.msg});
						commonService.processMsg($scope.errors, 'error');
					}else{
						commonService.setResponse(response);
						$location.path("/resultEntityScreening");
					}					
				});
			}
					
		}
		
		$scope.deleteEntity = function(idx){
			$scope.entities.splice(idx, 1);
		}
		
		$scope.editEntity = function(idx){
			var entityObj = $scope.entities[idx];
			$scope.entity = entityObj;
			//$('.selectpicker').selectpicker('refresh');
			//$scope.entity.entityType = Object.keys($scope.entityTypes)[Object.values($scope.entityTypes).indexOf($scope.entity.entityType)];
		}
		

		$scope.wlsFormChanged = function(){
			$scope.entity.entityCode = '';
		}
    }
]);


screeningController.controller('entityScreeningResultCtlr', ['$scope', '$location', '$http', '$compile', 'entityScreeningFactory', 'commonService', 'masterService',  
       function($scope, $location, $http, $compile, entityScreeningFactory, commonService, masterService) {
			
			//$scope.hits = 0;
			$scope.wlsScreenings = [];

			$scope.init = function(){
				var response = commonService.getResponse();
				
				if($.isEmptyObject(response)){
					$location.path("/createEntityScreening");
				}
				
				console.log("-------------------------");
				console.log(response);
				
				//$scope.hits = response.data[0].httsReturned.hits;
				//$scope.wlsScreenings = response.data[0].httsReturned.wlsList.wlsScreenings;
				
				$scope.wlsScreenings = response.data;
				$scope.clearMessages();
				
				commonService.setResponse(null);
			}
			
			$scope.showAudit = function(entityType, entityCode){
				if(entityType == 'Exporter'){
					masterService.wlsAuditHistory(entityCode, 'EXPORTER');
				}else if(entityType == 'Consignee'){
					masterService.wlsAuditHistory(entityCode, 'CONSIGNEE');
				}
				//alert(entityType+" - "+entityCode);
			}
			
				
}]);

restControllers
		.controller(
				'bookingCtrl',
				function($scope, $http, $rootScope, $cookies, $location,
						$window, productService, bookingService, messageService,
						commonService) {
					$scope.bookings = [];
					$scope.products = [];
					$scope.makesData = [];
					$scope.distances = [];
					$scope.makes = [];
					$scope.models = [];
					$scope.errors = [];
					$scope.bookingDetails = [];
					$scope.locationFrom = [];
					 $scope.mindate = new Date();
					$scope.locationTo= [];
					$scope.locationToFromReq= [];
				   var CLIENT_ID = "405935686262-3evd3vn14rjpiv89p9olnbd4cta31086.apps.googleusercontent.com";
	    function myFunc()
	    {
	        gapi.signin2.render('my-signin2', {
	        'width': 240,
	        'height': 50,
	        'longtitle': true,
	         'client_id':CLIENT_ID,
	        'theme': 'dark',      
	      }); 
  }
        $scope.renderButton = function() {
           gapi.signin2.render('my-signin2', {
	        'scope': 'profile email', 
	        'client_id':CLIENT_ID,
	        'onsuccess': onSignIn        
	      });
    }
          function onSignIn(googleUser) { 
     
     var profile = googleUser.getBasicProfile();
        //get
        var id_token = googleUser.getAuthResponse().id_token;
        var obj = {};
        obj.idtoken = id_token;
        //console.log(obj)
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='+id_token);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
          var response = JSON.parse(xhr.responseText);
           //console.log("checking ")
            //console.log(response)
          if(response.aud == CLIENT_ID)
          {
              if(response.iss == "accounts.google.com" || iss == "https://accounts.google.com")
              {
    
                var obj1 = {};
                obj1.email = response.email;
                obj1.user_name = response.name;
                obj1.login_type= "google";
                //console.log(obj1);
                    var xhr1 = new XMLHttpRequest();
                    xhr1.open('POST', '/pos/SetGoogleUser');
                    xhr1.setRequestHeader('Content-Type', 'application/json');
                    xhr1.onload = function() {
                    
                    if(xhr1.responseText == "success")
                    {
              +          $scope.syncCart(obj1);                   
                        $scope.checkSession();                    
                      //window.location.href = "/pos/home"
                      alert("fdsfd");
                    }
                    else if(xhr1.responseText == "bug")
                    {
                      alert("bug")
                        //alert("Something went wrong ! Contact Support") ;
                        $.notify("Something went wrong ! Contact Support",'error');                
                    }
                    else if(xhr1.responseText =="unauthorised")
                    {
                         //alert("Login Failed");    
                        $.notify("Login Failed",'error');      
                    }
                     blockUI.stop();
                };
                xhr1.send(JSON.stringify(obj1));
              }
              else
              {
                  $.notify("Something went wrong ! Contact Support",'error');      
              }
          }
          else
          {
           // alert("Something went wrong")
            $.notify("Oops, Something went wrong",'error');      
          }
        };
        xhr.send();
    
    }
	
					
					 $scope.changedValue = function(item) {
					   angular.forEach($scope.makesData, function(value, key){
					   
					   if(value.make==item) {
					      $scope.models =value.model;
					     }
   });
					 
  } 
  $scope.changeFromLocation = function(item) {
  					 
  } ;
  
    $scope.onDateChanged = function() {
    
    					 
  } ;
  
					$scope.getAllBookingsByUser = function() {
						usrmgtLhs('support-li');
						bookingService
								.fetchMakeToModelMapping().then(
										function(success) {
										$scope.makesData=success.data.data;
					
									$scope.locationFrom =$scope.makesData.locationFrom;
										angular.forEach($scope.makesData, function(value, key)
									{
									  	if(value.locationFrom!=undefined) {
											$scope.locationFrom =value.locationFrom;
											  $scope.locationTo=value.locationFrom;

										}
					if(value.locationTo!=undefined) {
											  $scope.locationTo=value.locationTo;
											  $scope.locationToFromReq=value.locationTo;

										}
										if(value.distances!=undefined) {
											$scope.distances=value.distances;
											$scope.locationTo=value.locationFrom;
										
										}
						
   });
   										bookingService
										.getBookingsToDisplay()
										.then(
												function(success) {
												alert(JSON.stringify(success.data.data));
													$scope.bookings = success.data.data;

													/*$(document).on(".payAdd_table_ticket tbody tr", 'click', function() {
													    var selected = $(this).hasClass("highlight");
													    $(".payAdd_table_ticket tr").removeClass("highlight");
													    if(!selected)
														    $(this).addClass("highlight");
													});*/

													productService
															.getAllProducts()
															.then(
																	function(success) {
																		$scope.products = success.data.data;
																	})
												},
												function(error) {
													console
															.log('error: data = ',
																	error);
												});

										},
												function(error1) {
													console
															.log('error: data = ',
																	error1);
												});
						
					};
					$scope.createBooking=function()
					  {
					  var timeForTravel;
					  	angular.forEach($scope.distances, function(value, key)
					  	{
					  	if((value.from==$scope.bookingDetails.pickUpLocation.substring(2) && value.to==$scope.bookingDetails.dropoffLocation.substring(2)) || (value.to==$scope.bookingDetails.pickUpLocation.substring(2) && value.from==$scope.bookingDetails.dropoffLocation.substring(2)))
					  		timeForTravel=value.time;
					  	});
									
					  if($scope.bookingDetails.pickUp ==undefined) {}
					     else {
					     var d1= new Date ( $scope.bookingDetails.pickUp );
					       var hours  = parseInt(timeForTravel) * 60 * 60 * 1000 ; 
					       var enddate  = new Date(d1).getTime() + hours ; 
					  	 $scope.date =enddate;
					  	 enddate= enddate;
					  $scope.bookingDetails.dropoff=enddate;
					     }
 
					  	bookingService.createBooking($cookies.get('username'),$scope.bookingDetails.pickUpLocation,
																$scope.bookingDetails.dropoffLocation,
																$scope.bookingDetails.make,
																$scope.bookingDetails.model,
																$scope.bookingDetails.pickUp,$scope.bookingDetails.dropoff).then(function(success) {alert("success");},
																	function(error) {
																		alert("error");
																	});
  };
					$scope.validateBooking= function() {
						$scope.errors.length = 0;

						if (!$scope.supportTicket.description.$valid) {
							var errorObj = $scope.supportTicket.description.$error;
							commonService.processNgError(errorObj,
									$scope.errors, 'supportTicket',
									'input[name=description]', 'Description');
							error = true;
						}
						if (!$scope.supportTicket.priority.$valid) {
							var errorObj = $scope.supportTicket.priority.$error;
							commonService.processNgError(errorObj,
									$scope.errors, 'supportTicket',
									'input[name=priority]', 'Priority');
							error = true;
						}
						if (!$scope.supportTicket.newMessage.$valid) {
							var errorObj = $scope.supportTicket.newMessage.$error;
							commonService.processNgError(errorObj,
									$scope.errors, 'supportTicket',
									'input[name=newMessage]', 'Message');
							error = true;
						}
						if ($scope.errors.length > 0) {
							return false;
						} else {
							return true;
						}
					}
					$scope.saveBooking= function() {
						var validated = $scope.validateBooking();
						if (validated) {
							bookingService
									.saveBooking(
											$cookies.get('username'),
											$scope.bookingItem.pickupLocation,
											$scope.bookingItem.dropoffLocation,
											$scope.bookingItem.vehicleMake,
											$scope.bookingItem.vehicleModel,
											$scope.bookingItem.pickupDate,
											
											$scope.bookingItem.dropoffDate)
									.then(
											function(success) {
												$scope.bookings = success.data.data;
												$scope.caseItem = {
													description : '',
													priority : '',
													ticketId : '',
													categoryDesc : ''
												};
												$scope.message = "Case added/updated.";
											},
											function(error) {
												if (error.status == 417) {
													var msgs = error.data.data;
													angular
															.forEach(
																	msgs,
																	function(
																			index,
																			value) {
																		$scope.errors
																				.push({
																					form : '',
																					field : '',
																					msg : index
																				});
																	});
												} else {
													$scope.errors
															.push({
																form : '',
																field : '',
																msg : 'Server encountered error, please try later.'
															});
												}
											});
						} else {
							commonService.processMsg($scope.errors, 'error');
						}
					};

					$scope.editCase = function(selectedCI) {
						var validated = $scope.validateCase();
						if (validated) {
							messageService
									.markMessageAsReadForCases(
											selectedCI.ticketId)
									.then(
											function(success) {
												$scope.caseItem = selectedCI;
												$scope.obj.messageCount = success.data.data;
												$scope.message = "";
											},
											function(error) {
												if (error.status == 417) {
													var msgs = error.data.data;
													angular
															.forEach(
																	msgs,
																	function(
																			index,
																			value) {
																		$scope.errors
																				.push({
																					form : '',
																					field : '',
																					msg : index
																				});
																	});
												} else {
													$scope.errors
															.push({
																form : '',
																field : '',
																msg : 'Server encountered error, please try later.'
															});
												}
											});
						} else {
							commonService.processMsg($scope.errors, 'error');
						}

					};

					$scope.closeCase = function(status) {
						var validated = $scope.validateCase();
						if (validated) {
							caseService
									.saveCase(
											$cookies.get('username'),
											$scope.caseItem.description,
											$scope.caseItem.priority,
											$scope.caseItem.ticketId,
											status,
											$scope.caseItem.selectedSubProduct.productId,
											$scope.caseItem.newMessage)
									.then(
											function(success) {
												$scope.bookings = success.data.data;
												$scope.caseItem = {
													description : '',
													priority : '',
													ticketId : '',
													categoryDesc : ''
												};
												if(status == 'C'){
													$scope.message = "Case Closed.";
												}else if(status == 'O'){
													$scope.message = "Case Open.";
												}
												
											},
											function(error) {
												if (error.status == 417) {
													var msgs = error.data.data;
													angular
															.forEach(
																	msgs,
																	function(
																			index,
																			value) {
																		$scope.errors
																				.push({
																					form : '',
																					field : '',
																					msg : index
																				});
																	});
												} else {
													$scope.errors
															.push({
																form : '',
																field : '',
																msg : 'Server encountered error, please try later.'
															});
												}
											});
						} else {
							commonService.processMsg($scope.errors, 'error');
						}

					};

					$scope.reset = function() {
						$scope.caseItem = {
							description : '',
							priority : '',
							ticketId : '',
							categoryDesc : ''
						};
						$scope.message = "";
					};

				});
site_app.controller('checkOutCtrl', function($scope, $rootScope,$http, $window, $sce, $filter, tailorService_site,blockUI)
{
    $scope.hide_cart =true;
    $scope.dataobj = {};
    $scope.customerObj ={};
    $scope.customerDetails = {};
    $scope.signupObj = {};
    $scope.loginObj ={};
    $scope.paymentObj ={};
    $scope.paymentObj.payment_type = "COD";
    $scope.session = "false";
    $scope.before_signup = "true";
    $scope.purchaseObj ={};
    $scope.finalObj = {};
    //disable
    $scope.address_active = false;


    var changeTab = function() {

  setTimeout(

    function()
    {
      $('button.tt').click(
        function()
        {
          var neededClass = $(this).attr('class');        
          $('.test-class').removeClass('active');
          $('.tab-pane').removeClass('active');
          if(neededClass.toLowerCase().indexOf("tofirsttab") >= 0)
          {
            $('#firstTab').addClass('active');
            $('#signin').addClass('active');
          }
          if(neededClass.toLowerCase().indexOf("tosecondtab") >= 0)
          {
            $('#secondTab').addClass('active');
            $('#address').addClass('active');

          }
          if(neededClass.toLowerCase().indexOf("tothirdtab") >= 0)
          {
            $('#thirdTab').addClass('active');
            $('#order_summary').addClass('active');
          }
          if(neededClass.toLowerCase().indexOf("tofourthtab") >= 0)
          {
            $('#fourthTab').addClass('active');
            $('#payment').addClass('active');
          }
        })

    },0);
    }

       // changeTab();

    $scope.checkSession = function()
    {

        blockUI.start();
          tailorService_site.httpRequest('/pos/checkSession', 'GET', '').then(function(data)
            {
                  blockUI.stop();
                if(data =="unauthorized")
                {
                    $scope.session = "false";                  
                }
                else if(data =="bug")
                {      
                    //alert("something went wrong ! Contact Support");
                    $.notify("Something went wrong ! Contact Support",'error');
                }
                else
                {
                    $scope.customerObj = data;                  
                    $scope.session ="true";
                }
                $scope.show_cart();
            });
    }

      $scope.show_cart = function()
  {
    tailorService_site.httpRequest('/pos/getcart', 'GET', '').then(function(data)
    {      
        $scope.c_list = data;
        if(data != "unauthorized")
        {
          if($scope.c_list.length ==0)
          {
           
           // window.location.href ="/pos/home"
          }
        }
        else
        {
          var obj ={};
          obj.ref_id = localStorage.getItem('ATT-CART-REF');
           tailorService_site.httpRequest('/pos/getcartGuest', 'POST', obj).then(function(data)
            {  
             $scope.cg_list = data;
             if($scope.cg_list.length ==0)
             {
              
              // window.location.href ="/pos/home"
             }
            });
        
        }
     });
  }

    $scope.updatePhoneNo = function()
    {
        if($scope.dataobj.phone_no !=null && $scope.dataobj.phone_no !=undefined && $scope.dataobj.phone_no !='')
        {
           blockUI.start();
           var obj ={};
           obj.phone_no = $scope.dataobj.phone_no;
           tailorService_site.httpRequest('/pos/updatePhoneNo', 'POST', obj).then(function(data)
            {   
                $scope.checkSession();         
                blockUI.stop();
               // alert(data)
            });
        }
        else
        {
            alert("Enter Phone Mno")
        }
    }

    $scope.checkEmail = function()
    {
         if($scope.dataobj.email !=null && $scope.dataobj.email !=undefined && $scope.dataobj.email !='')
        {

             blockUI.start();
             var obj = {};
             obj.email  =$scope.dataobj.email;
           tailorService_site.httpRequest('/pos/checkEmail', 'POST', obj).then(function(data)
            {
                 blockUI.stop();
                 if(data =="signup")
                 {
                    $scope.signupObj.email = obj.email;
                    $('#signup_modal').modal('show');
                    
                 }
                  if(data =="login")
                 {
                    $scope.loginObj.email = obj.email;
                    $('#login_modal').modal('show');
                 }
                 else
                 {
                   // alert(data);

                 }
            });
        }
        else
        {
            $.notify("Please enter valid email",'warning');
        }
    }

    $scope.signup = function()
    {
     var form = $('#signup').find('.form-validation');
      var valid = form.valid();
        if(!valid)
        {
            form.data('validator').focusInvalid();
            return false;
        }
        else
        {
          if($scope.signupObj.password == $scope.signupObj.confirm_password)
          {
            blockUI.start();
               $('#signup_but').button('loading');
              $('#signup_but').prop('disabled', true);
              tailorService_site.httpRequest('/pos/customer/signup', 'POST', $scope.signupObj).then(function(data)
              {
                 blockUI.stop();
                  $('#signup_modal').modal('hide');
                // alert(data);
                 $scope.before_signup = "false";
              });
          }
          else
          {
            //bootbox.alert("Both Passwords Must Be Same");
            $.notify("Passwords Dont Match",'warning');
          }
        }
      }
    $scope.login = function()
    {
         var form = $('#login').find('.form-validation');
         var valid = form.valid();
            if(!valid)
            {
                form.data('validator').focusInvalid();
                return false;
            }
            else
            {
                blockUI.start();
                $('#save').button('loading');
                $('#save').prop('disabled', true);
                tailorService_site.httpRequest('/pos/checkOutlogin', 'POST', $scope.loginObj).then(function(data)
                {
                     blockUI.stop();
                     if(data =="bug")
                     {
                        //alert("Something went wrong ! Contact Support !");
                        $.notify("Something went wrong ! Contact Support",'error');
                     }
                     else if(data =="failed")
                     {

                        //$scope.login_msg = "Invalid Email or Password";  
                        $.notify("Invalid Email or Password",'warning'); 
                     }
                     else
                     {
                        $scope.session = "true";
                        $scope.customerObj =data;
                        $('#login_modal').modal('hide');
                     }
                     
                });
            }
    }

    $scope.fetchCustDetails = function()
    {
      changeTab();
        //blockUI.start();
        tailorService_site.httpRequest('/pos/fetchCustDetails', 'GET', '').then(function(data)
        {
            //blockUI.stop();
            $scope.customerDetails = data;
            
        });
    }

    $scope.saveAddress = function()
    {
      //console.log($scope.customerDetails)
       var form = $('#address_val').find('.form-validation');
         var valid = form.valid();
            if(!valid)
            {
                form.data('validator').focusInvalid();
                return false;
            }
            else
            {
                blockUI.start();
                $('#save').button('loading');
                $('#save').prop('disabled', true);
                tailorService_site.httpRequest('/pos/updateCustDetails', 'POST',$scope.customerDetails).then(function(data)
                {

                     blockUI.stop();
                     if(data =="bug")
                     {
                        alert("Something went wrong ! Contact Support !");
                        $.notify("Something went wrong ! Contact Support",'error');
                     }
                     else if(data =="unauthorized")
                     {

                        //alert("Session Expired ! Login Again");   
                        $.notify("Session Expired ! Login Again",'error');
                     }
                     else
                     {  
                        $scope.session = "true";
                        $scope.generateSummary();
                        changeTab();
                        $scope.customerObj =data;                        
                     }
                     
                });
            }  
    }

    $scope.generateSummary =function()
    {
        blockUI.start();
        var localGuest =localStorage.getItem("IS_GUEST");
        $scope.generateSummary.GuestId=localStorage.getItem("ATT-CART-REF")
        alert($scope.generateSummary.GuestId);
        if(localGuest=="TRUE")
{       	tailorService_site.httpRequest('/pos/generateGuestSummary/'+$scope.generateSummary.GuestId, 'GET',$scope.generateSummary).then(function(data)
        {
    blockUI.stop();
    $scope.summary = data;
    $scope.final_total();
    if(data.cart.length ==0)
    {               
        window.location.href ="/pos/home";
    }
});
        	
        	}
	        else{        	tailorService_site.httpRequest('/pos/generateSummary', 'GET',$scope.generateSummary).then(function(data)
	        {
	            blockUI.stop();
	            $scope.summary = data;
	            $scope.final_total();
	            if(data.cart.length ==0)
	            {               
	                window.location.href ="/pos/home";
	            }
	        });
	        }
                    
    }
    function add(a, b)
     {
      //alert(a)
      //alert(b.product_price)
        return parseFloat(a) + parseFloat(b.product_price);
    }

    $scope.getProductTotal = function()
    {
        var sum = $scope.summary.cart.reduce(add, 0);
        return sum;
    }
     function add(a, b)
     {
        return parseFloat(a) + parseFloat(b.product_price);
     }

    $scope.discount_price = function()
    {
        var diff = 0;
       for (var i = 0; i < $scope.summary.cart.length; i++) {
            var obj =  $scope.summary.cart[i]
            diff = parseFloat(obj.product_price) - parseFloat(obj.discounted_price) + parseFloat(diff) ;

       }
       return diff;
    }

    $scope.final_total = function()
    {
      $scope.product_total_price = 0;
      $scope.product_total_price_kids = 0;
      $scope.final_product_price = 0;
      $scope.final_product_price_kids = 0;
      for(var i=0;i<$scope.summary.cart.length;i++)
      {
        var obj =  $scope.summary.cart[i]; 
        if(obj.purchasing_for_kids == 1)
        {
          obj.kids_size = JSON.parse(obj.kids_size);
        }

        $scope.final_product_price =  ( parseFloat(obj.discounted_price) +  parseFloat($scope.final_product_price) ) *(parseFloat(obj.qty));
        if(obj.purchasing_for_kids == 1)
          $scope.final_product_price_kids =  ( parseFloat(obj.discounted_price_kids) +  parseFloat($scope.final_product_price_kids) ) *(parseFloat(obj.kids_qty));
        
        $scope.final_product_price =  $scope.final_product_price + $scope.final_product_price_kids;
        $scope.product_total_price =   (parseFloat(obj.product_price) +  parseFloat($scope.product_total_price) )*(parseFloat(obj.qty)) ;
        
        if(obj.purchasing_for_kids == 1)
          $scope.product_total_price_kids = (parseFloat(obj.product_price_kids) +  parseFloat($scope.product_total_price) )*(parseFloat(obj.kids_qty)) ;
        
        $scope.product_total_price = $scope.product_total_price + $scope.product_total_price_kids;

        var order_total = parseFloat(obj.discounted_price) * parseFloat(obj.qty)
        obj.shipping_charges = $scope.getShippingCharges(order_total);
      }
        
      $scope.total_discount_price = parseFloat(obj.discount_amount) * parseFloat(obj.qty);
      if(obj.purchasing_for_kids == 1)
        $scope.total_discount_price =parseFloat($scope.total_discount_price) - parseFloat(obj.discount_amount) * parseFloat(obj.qty);

        //$scope.product_discounted_price = $scope.product_total_price - $scope.final_product_price;
        if(parseFloat($scope.final_product_price) <=2000)
          $scope.shipping_charges = 25;
        else if(parseFloat($scope.final_product_price) <=5000)
           $scope.shipping_charges = 50;
        else if(parseFloat($scope.final_product_price) <=7500)
           $scope.shipping_charges = 75;
       else
          $scope.shipping_charges = 100;

        $scope.grand_total = parseFloat($scope.final_product_price) + parseFloat($scope.shipping_charges) ;
    }
    $scope.getShippingCharges = function(total)
    {
      var shipping;
       if(parseFloat(total) <=2000)
          shipping = 25;
        else if(parseFloat(total) <=5000)
           shipping = 50;
        else if(parseFloat(total) <=7500)
           shipping = 75;
       else
          shipping = 100;
        return shipping;
    }

      $scope.qty_change = function(item,op)
  {
    //console.log(item)
    var qty_flag =1;
    if(op =="plus")
    {
      item.qty = parseFloat(item.qty) + 1;
    }
    else
    {
      if(item.qty > 1)
      {
        item.qty = parseFloat(item.qty) - 1;
      }
      else
      {
        qty_flag  =0;
         //$rootScope.alertmessage = "Qty Cannot Be Less Than 1";
         $.notify("Quantity Cannot Be less than 1",'warning');
            //$('#alertmodal').modal('show');
      }
    }
    if(qty_flag ==1)
    {
        tailorService_site.httpRequest('/pos/updateCartQty/'+item.cart_id+'/'+item.qty, 'GET', '').then(function(data)
        {

            if(data =="unauthorized")
            {
              var local = localStorage.getItem('ATT-CART-REF');
                tailorService_site.httpRequest('/pos/updateCartQtyGuest/'+item.cart_id+'/'+item.qty+'/'+local, 'GET', '').then(function(data)
              {
                  $scope.generateSummary();
              });
            }
            else
             $scope.generateSummary();
        });
    }
  }

  $scope.remove_from_cart = function(item)
  {
    //alert(item.cart_id)
    tailorService_site.httpRequest('/pos/removefromcart/'+item.cart_id, 'GET', '').then(function(data)
        {
          if(data =="success")
          {
            $scope.show_cart();
            //$rootScope.alertmessage = "Product "+item.product_name+" Succesfully Removed From Cart";
            $.notify("Product Succesfully Removed",'success');
            //$('#alertmodal').modal('show');

          }
          else if("unauthorized")
          {
            $scope.remove_from_cart_guest(item);
          }
           $scope.generateSummary();
        });
  }
  $scope.remove_from_cart_guest = function(item)
  {
    var ref_id = localStorage.getItem('ATT-CART-REF');
    tailorService_site.httpRequest('/pos/removefromcartGuest/'+item.cart_id+'/'+ref_id, 'GET', '').then(function(data)
        {
          if(data =="success")
          {
            $scope.show_cart();
            //$rootScope.alertmessage = "Product "+item.product_name+" Succesfully Removed From Cart";
            $.notify("Product Succesfully Removed",'success');
            //$('#alertmodal').modal('show');
             $scope.generateSummary();
          }
          
        });
  }

  $scope.submitOrder = function()
  {      
    if($scope.paymentObj.payment_type == "COD")
    {
        tailorService_site.httpRequest('/pos/finalPurchase', 'POST', $scope.paymentObj).then(function(data)
        {

        });
    }
       
  }

  $scope.testpay = function()
  {
    $scope.finalObj.totalAmount = $scope.product_total_price;
    $scope.finalObj.shippingCharges = $scope.shipping_charges;
    $scope.finalObj.billAmount = $scope.grand_total;    
    $scope.finalObj.paymentType= $scope.paymentObj.payment_type;

    $scope.finalObj.customerObj = $scope.customerDetails;
    var obj ={};
    blockUI.start();
    //obj.amount = $scope.final_total();
     tailorService_site.httpRequest('/pos/testPay', 'POST', $scope.finalObj).then(function(data)
        {
           blockUI.stop();
           if($scope.paymentObj.payment_type == 'COD')
           {
              alert(data)
           }
          else if($scope.paymentObj.payment_type == "CARD")
           {
             window.location.href = data;
           }
        });
  }
    



    //social login stuff 

    var CLIENT_ID = "232597724291-g0822shnvfj0kp23ro2ov9s4r3bku2j2.apps.googleusercontent.com";
    function myFunc()
    {
        blockUI.start();
        gapi.signin2.render('my-signin2', {
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',      
      }); 
        blockUI.stop();
    }

      $scope.renderButton = function() {
        blockUI.start();
      gapi.signin2.render('my-signin2', {
        'scope': 'profile email',                
        'onsuccess': onSignIn        
      });
      blockUI.stop();
    }

        function onSignIn(googleUser) { 
        blockUI.start();   
        var profile = googleUser.getBasicProfile();
      /*  var id_token = googleUser.getAuthResponse().id_token;
        var obj = {};
        obj.idtoken = id_token;
        //console.log(obj)
          var xhr = new XMLHttpRequest();
          xhr.open('POST', '/pos/GoogleAuth');
          xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
          xhr.onload = function() {
          //console.log('Signed in as: ' + xhr.responseText);
        };
        xhr.send('id_token='+id_token);*/

    //get
    var id_token = googleUser.getAuthResponse().id_token;
    var obj = {};
    obj.idtoken = id_token;
    //console.log(obj)
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='+id_token);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
      var response = JSON.parse(xhr.responseText);
       //console.log("checking ")
        //console.log(response)
      if(response.aud == CLIENT_ID)
      {
          if(response.iss == "accounts.google.com" || iss == "https://accounts.google.com")
          {

            var obj1 = {};
            obj1.email = response.email;
            obj1.user_name = response.name;
            obj1.login_type= "google";
            //console.log(obj1);
                var xhr1 = new XMLHttpRequest();
                xhr1.open('POST', '/pos/SetGoogleUser');
                xhr1.setRequestHeader('Content-Type', 'application/json');
                xhr1.onload = function() {
                
                if(xhr1.responseText == "success")
                {
                    $scope.syncCart(obj1);                   
                    blockUI.stop();
                    $scope.checkSession();                    
                  //window.location.href = "/pos/home"
                }
                else if(xhr1.responseText == "bug")
                {
                  alert("bug")
                    blockUI.stop();   
                    //alert("Something went wrong ! Contact Support") ;
                    $.notify("Something went wrong ! Contact Support",'error');                
                }
                else if(xhr1.responseText =="unauthorised")
                {
                     blockUI.stop();
                     //alert("Login Failed");    
                    $.notify("Login Failed",'error');      
                }
                 blockUI.stop();
            };
            xhr1.send(JSON.stringify(obj1));
          }
          else
          {
             blockUI.stop();
             //alert("Something went wrong ! Contact Support")
              $.notify("Something went wrong ! Contact Support",'error');      
          }
      }
      else
      {
        blockUI.stop();
       // alert("Something went wrong")
        $.notify("Oops, Something went wrong",'error');      
      }
    };
    xhr.send();

}

  


/*  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {

    if (response.status === 'connected') {
      testAPI();
    } else if (response.status === 'not_authorized') {      
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  $scope.checkLoginState = function() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }
*/
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '526412884213896',
    cookie     : true,  // enable cookies to allow the server to access 
    status     : false  ,               // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.5' // use graph api version 2.5
  });

 

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  /*function testAPI() {
    //console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=name,email', function(response) {
          var obj1 = {};
            obj1.email = response.email;
            obj1.user_name = response.name;
            obj1.login_type= "facebook";
            //console.log(obj1);
                var xhr1 = new XMLHttpRequest();
                xhr1.open('POST', '/pos/SetGoogleUser');
                xhr1.setRequestHeader('Content-Type', 'application/json');
                xhr1.onload = function() {
                if(xhr1.responseText == "success")
                {
                 $scope.checkSession()
                }
            };
            xhr1.send(JSON.stringify(obj1));
    });
  }*/


  $scope.renderGuestButton = function() {
      blockUI.start();
   
      var obj1 = {};
      obj1.email = "Guest";
      obj1.user_name = "Guest";
      obj1.login_type= "Guest";
      obj1.idtoken = 12345;
      $scope.syncGuestCart(obj1);                   
      $scope.session="true";
      $scope.customerObj.email_address="Guest";
      blockUI.stop();
    
  }

  $scope.syncGuestCart = function(obj)
  {
  	
  var syncObj ={};
  syncObj.email = obj.email;

    var local;
     local = localStorage.getItem('ATT-CART-REF');
     syncObj.ref_id = local;
     var measurement = localStorage.getItem("MEASUREMENT");
     if(local !=null)
     {
  	   alert("ll");
      blockUI.start();
         tailorService_site.httpRequest('/pos/syncGuestCart', 'POST', syncObj).then(function(data)
          {
               blockUI.stop();
               alert("data"+data);
                  if(data=="success")
                  {
                      //alert("Sync Completed")
                       $.notify("Sync Completed",'success');      
                       $scope.customerDetails.cust_id=localStorage.getItem("ATT-CART-REF");
                       alert($scope.customerDetails.cust_id);
//                       localStorage.removeItem("ATT-CART-REF");
                       localStorage.setItem("IS_GUEST","TRUE");
                       alert(localStorage.getItem("IS_GUEST"));
                     }
                  else if(data == "bug")
                  {
                      blockUI.stop();
                      //alert("Something Went Wrong ! COntact Support");
                      $.notify("Something went wrong ! Contact Support",'error');      
                  }
          }); 
     }


  }    





  //test
   $scope.fb_login =function(){
    blockUI.start();
    FB.login(function(response) {

        if (response.authResponse) {
            //console.log('Welcome!  Fetching your information.... ');
            ////console.log(response); // dump complete info
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID

            FB.api('/me?fields=name,email', function(response) {
                var obj1 = {};
            obj1.email = response.email;
            obj1.user_name = response.name;
            obj1.login_type= "facebook";
            if(!obj1.email)
                alert("this Account Does Not hav A Valid Email Address")
            //console.log(obj1);
                var xhr1 = new XMLHttpRequest();
                xhr1.open('POST', '/pos/SetGoogleUser');
                xhr1.setRequestHeader('Content-Type', 'application/json');
                xhr1.onload = function() {
                if(xhr1.responseText == "success")
                {
                    $scope.syncCart(obj1); 
                    $scope.checkSession();
                }
            };
            xhr1.send(JSON.stringify(obj1));
                //get user email
          // you can store this data into your database             
            });
            blockUI.stop();

        } else {
            blockUI.stop();
            //user hit cancel button
            //console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'public_profile,email'
    });
}


//sync The Cart
$scope.syncCart = function(obj)
{
var syncObj ={};
syncObj.email = obj.email;

  var local;
   local = localStorage.getItem('ATT-CART-REF');
   syncObj.ref_id = local;
   if(local !=null)
   {
    blockUI.start();
       tailorService_site.httpRequest('/pos/syncCart', 'POST', syncObj).then(function(data)
        {
             blockUI.stop();
                if(data=="success")
                {
                    //alert("Sync Completed")
                     $.notify("Sync Completed",'success');      
                    localStorage.removeItem("ATT-CART-REF");
                    $scope.checkSession();
                }
                else if(data == "bug")
                {
                    blockUI.stop();
                    //alert("Something Went Wrong ! COntact Support");
                    $.notify("Something went wrong ! Contact Support",'error');      
                }
        }); 
   }


}    

$scope.logout_checkout = function()
{
   tailorService_site.httpRequest('/pos/logout', 'GET', '').then(function(data)
        {
            $scope.checkSession();
           // $scope.session = "false";
        });
} 

});
checkout.js
Displaying checkout.js.
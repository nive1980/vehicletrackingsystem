package com.vehicleTracking.service;

import java.util.Collection;

import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Booking;
import com.vehicleTracking.model.Vehicle;

public interface BookingService {
	public void saveBooking(Booking newCase) throws ApplicationException;
	public Booking getBooking(long caseId) throws ApplicationException;
	public Collection<Booking> getBookingsByUser(long userId)throws ApplicationException;
	public boolean isVehicleAlreadyBookedForPeriod(Vehicle v, String string);
}

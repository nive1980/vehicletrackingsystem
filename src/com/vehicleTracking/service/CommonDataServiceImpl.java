package com.vehicleTracking.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vehicleTracking.dao.GenericDao;
import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.helper.CommonDataHelper;
import com.vehicleTracking.model.Distance;
import com.vehicleTracking.model.Location;
import com.vehicleTracking.model.VehicleMake;
import com.vehicleTracking.model.VehicleModel;

@Service("commonDataService")
public class CommonDataServiceImpl implements CommonDataService{

	@Autowired
	private GenericDao dao;	
	
		@Override
	public Collection<VehicleMake> getVehicleMakes()
			throws ApplicationException {
		String queryString = "select Object(make)  from VehicleMake make";
		Object[] params = {};
		Collection<VehicleMake> makes = null;
		try{
			makes = dao.findDynamicQuery(queryString, params);
		}catch(Exception e){
			e.printStackTrace();
			throw new ApplicationException("Error getting SBU list", e);
		}
		
		return makes;
	}

	@Override
	public Collection<VehicleModel> getVehicleModels(long makeId)
			throws ApplicationException {
		String queryString = "select Object(model)  from VehicleModel model where model.make=?1 ";
		Object[] params = {makeId};
		Collection<VehicleModel> models = null;
		try{
			models = dao.findDynamicQuery(queryString, params);
		}catch(Exception e){
			e.printStackTrace();
			throw new ApplicationException("Error getting SBU list", e);
		}
		
		return models;	}

	@Override
	public Collection<Location> getLocations() throws ApplicationException{
		String queryString = "select Object(location)  from Location location";
		Object[] params = {};
		Collection<Location> locations = null;
		try{
			locations = dao.findDynamicQuery(queryString, params);
			System.out.println("locations "+locations.size());
		}catch(Exception e){
			e.printStackTrace();
			throw new ApplicationException("Error getting SBU list", e);
		}
		
		return locations;
	}
	@Override
	public Collection<Distance> getDistances() throws ApplicationException{
		String queryString = "select Object(distance)  from Distance distance";
		Object[] params = {};
		Collection<Distance> distances = null;
		try{
			distances = dao.findDynamicQuery(queryString, params);
			System.out.println("distances "+distances.size());
		}catch(Exception e){
			e.printStackTrace();
			throw new ApplicationException("Error getting SBU list", e);
		}
		
		return distances;
	}
	
	/*@Override
	public boolean validateCountry(String countryCode) throws ApplicationException {
		try{
			Country country = (Country) dao.read(Country.class, countryCode);
			if(country==null){
				return false;
			}else{
				return true;
			}
		}catch(Exception e){
			throw new ApplicationException("Error validating country", e);
		}
	}

	@Override
	public boolean validateMot(String motCode) throws ApplicationException {
		try{
			ModeOfTransport country = (ModeOfTransport) dao.read(ModeOfTransport.class, motCode);
			if(country==null){
				return false;
			}else{
				return true;
			}
		}catch(Exception e){
			throw new ApplicationException("Error validating ModeOfTransport", e);
		}
	}
	
	@Override
	public boolean validateExporter(String expCode) throws ApplicationException {
		try{
			ExporterValue exporter = (ExporterValue) dao.read(ExporterValue.class, expCode);
			if(exporter==null){
				return false;
			}else{
				return true;
			}
		}catch(Exception e){
			throw new ApplicationException("Error validating ExporterValue", e);
		}
	}

	
	
	@Override
	public boolean validateConsignee(String consigneeCode) throws ApplicationException {
		try{
			ConsigneeValue consignee = (ConsigneeValue) dao.read(ConsigneeValue.class, consigneeCode);
			if(consignee==null){
				return false;
			}else{
				return true;
			}
		}catch(Exception e){
			throw new ApplicationException("Error validating ConsigneeValue", e);
		}
	}
	
	@Override
	public boolean validateFreightForwarder(String ffCode) throws ApplicationException {
		try{
			FreightForwarderValue ff = (FreightForwarderValue) dao.read(FreightForwarderValue.class, ffCode);
			if(ff==null){
				return false;
			}else{
				return true;
			}
		}catch(Exception e){
			throw new ApplicationException("Error validating FreightForwarderValue", e);
		}
	}
	
	
	@Override
	public boolean validateSchb(String schbCode) throws ApplicationException {
		try{
			SchBValue schb = (SchBValue) dao.read(SchBValue.class, schbCode);
			if(schb==null){
				return false;
			}else{
				return true;
			}
		}catch(Exception e){
			throw new ApplicationException("Error validating FreightForwarderValue", e);
		}
	}
	
	@Override
	public boolean validateModel(String modelCode) throws ApplicationException {
		try{
			Model schb = (Model) dao.read(Model.class, modelCode);
			if(schb==null){
				return false;
			}else{
				return true;
			}
		}catch(Exception e){
			throw new ApplicationException("Error validating Model", e);
		}
	}
	
	
	@Override
	public <E> boolean validateEntity(E classType, String modelCode) throws ApplicationException {
		try{
			E entity = (E) dao.read(classType.getClass(), modelCode);
			if(entity==null){
				return false;
			}else{
				return true;
			}
		}catch(Exception e){
			throw new ApplicationException("Error validating Entity", e);
		}
	}*/
}

package com.vehicleTracking.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.vehicleTracking.dao.GenericDao;
import com.vehicleTracking.dto.BookingDto;
import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Location;
import com.vehicleTracking.model.Vehicle;
import com.vehicleTracking.model.VehicleMake;
import com.vehicleTracking.model.VehicleModel;


@Service
@Transactional(propagation = Propagation.REQUIRED)
public class VehicleServiceImpl implements VehicleService {
	@Autowired
	private GenericDao dao;

	public GenericDao getDao() {
		return dao;
	}

	public void setDao(GenericDao dao) {
		this.dao = dao;
	}
		@Override
	public Vehicle getVehicle(long id) throws ApplicationException {
		return (Vehicle)dao.read(Vehicle.class,id);
	}

		@Override
		public String getVehicleMake(long vehicleId) throws ApplicationException {
			Vehicle vehicle=(Vehicle)dao.read(Vehicle.class,vehicleId);
			VehicleModel model= (VehicleModel)dao.read(VehicleModel.class, vehicle.getModel());
			VehicleMake make =(VehicleMake)dao.read(VehicleMake.class, model.getMake());
			return make.getName();
		}

		@Override
		public String getVehicleModel(long vehicleId) throws ApplicationException {
			Vehicle vehicle=(Vehicle)dao.read(Vehicle.class,vehicleId);
			VehicleModel model= (VehicleModel)dao.read(VehicleModel.class, vehicle.getModel());
			return model.getName();
		}

		@Override
		public List<Vehicle> getVehicle(BookingDto bookingDto,CommonDataService commonService) {
			// TODO Auto-generated method stub
			String queryString = "select Object(v) from Vehicle v where model = ?1 and status='A'";
			long modelid=Long.parseLong(getVehicleModelByName(bookingDto.getVehicleModel()));
			System.out.println("csdds "+modelid);
					Object[] params = { modelid };
			List<Vehicle> vehicles =(List<Vehicle> ) dao.findDynamicQuery(queryString, params);
			return vehicles;
		
		}

		private String getVehicleModelByName(String vehicleModel) {
			String queryString = "select Object(vm) from VehicleModel vm where name = ?1";
			Object[] params = { vehicleModel };
			List<VehicleModel> vehicles =(List<VehicleModel> ) dao.findDynamicQuery(queryString, params);
			String modelId=null;
			if(vehicles!=null && vehicles.size()>0)
				modelId=""+vehicles.get(0).getId();
			return modelId;
		}


}

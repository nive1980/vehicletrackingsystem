package com.vehicleTracking.service;

import java.util.Collection;
import java.util.List;

import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Country;
import com.vehicleTracking.model.User;


public interface CountryService {

	
	public Collection<Country> getCountries() throws ApplicationException;
}

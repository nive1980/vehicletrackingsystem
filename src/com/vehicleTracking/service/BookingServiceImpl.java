package com.vehicleTracking.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.vehicleTracking.dao.GenericDao;
import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Booking;
import com.vehicleTracking.model.Vehicle;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class BookingServiceImpl implements BookingService {
	@Autowired
	private GenericDao dao;

	public GenericDao getDao() {
		return dao;
	}

	public void setDao(GenericDao dao) {
		this.dao = dao;
	}
	@Override
	public void saveBooking(Booking newCase) throws ApplicationException{
		try{
			dao.create(newCase);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException("Booking Not created ", e);
		}
	}

	@Override
	public Booking getBooking(long caseId) throws ApplicationException {
		return (Booking)dao.read(Booking.class, caseId);
	}

	@Override
	public Collection<Booking> getBookingsByUser(long userId) throws ApplicationException {
		try{
			// ORDER BY c.createdOn DESC"
			String queryString = "select Object(b) from Booking b where b.userId = ?1 ";
			Object[] params = {userId};
			return dao.findDynamicQuery(queryString, params);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException("Cases Not found ", e);
		}
	}

	@Override
	public boolean isVehicleAlreadyBookedForPeriod(Vehicle v,String pickUpDate) {
		// TODO Auto-generated method stub
		String FORMATER = "MM/dd/yy HH:mm aa";

	    DateFormat format = new SimpleDateFormat(FORMATER);
	    boolean isVehicleAlreadyBooked =false;
	    try {
			Date dt=format.parse(pickUpDate);
			Collection<Booking> bookings = getBookingsForVehicle(v);
			System.out.println("vdfds "+bookings.size());
			System.out.println("dt "+dt);
			for(Booking b:bookings) {
				System.out.println("b "+b.getBookingId());
				
				Date dateBookedFrom=b.getPickUpDate();
				Date dateBookedTo =b.getDropoffDate();
				isVehicleAlreadyBooked=dt.after(dateBookedFrom) && dt.before(dateBookedTo);
				if(isVehicleAlreadyBooked)
					break;
			}
			
	    } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return isVehicleAlreadyBooked;
	}

	private Collection<Booking>  getBookingsForVehicle(Vehicle v) {
		String query="select Object(b) from Booking b where b.vehicleId = ?1 ";
		Object[] params = {v.getId()};
		System.out.println("vehicle "+v.getId());
		return   dao.findDynamicQuery(query, params);
	}


}

package com.vehicleTracking.service;

import java.util.Collection;

import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Booking;

import com.vehicleTracking.model.Location;

public interface LocationService {
	public Location getLocation(long id) throws ApplicationException;
}

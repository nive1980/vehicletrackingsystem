package com.vehicleTracking.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.vehicleTracking.dao.GenericDao;
import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Booking;

import com.vehicleTracking.model.Location;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class LocationServiceImpl implements LocationService {
	@Autowired
	private GenericDao dao;

	public GenericDao getDao() {
		return dao;
	}

	public void setDao(GenericDao dao) {
		this.dao = dao;
	}
		@Override
	public Location getLocation(long id) throws ApplicationException {
		return (Location)dao.read(Location.class, id);
	}


}

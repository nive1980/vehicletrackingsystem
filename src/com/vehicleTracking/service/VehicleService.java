package com.vehicleTracking.service;

import java.util.Collection;
import java.util.List;

import com.vehicleTracking.dto.BookingDto;
import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Vehicle;
import com.vehicleTracking.model.VehicleMake;

public interface VehicleService {
	public Vehicle getVehicle(long id) throws ApplicationException;
	public String getVehicleMake(long vehicleId) throws ApplicationException;
	public String getVehicleModel(long vehicleId) throws ApplicationException;
	public List<Vehicle> getVehicle(BookingDto bookingDto,CommonDataService commonService);
}

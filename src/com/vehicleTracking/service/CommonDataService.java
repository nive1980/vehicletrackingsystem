package com.vehicleTracking.service;

import java.util.Collection;

import com.vehicleTracking.model.Location;
import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Distance;
import com.vehicleTracking.model.VehicleMake;
import com.vehicleTracking.model.VehicleModel;

public interface CommonDataService {

	public Collection<VehicleMake> getVehicleMakes() throws ApplicationException;
	public Collection<VehicleModel> getVehicleModels(long makeId) throws ApplicationException;
	public Collection<Location> getLocations()  throws ApplicationException;
	Collection<Distance> getDistances() throws ApplicationException;

	
}

package com.vehicleTracking.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Country;
import com.vehicleTracking.service.CommonDataService;
import com.vehicleTracking.service.GenericService;

@Component
public class CommonDataHelper {

	@Autowired
	private CommonDataService service;
	
	@Autowired
	private GenericService genericService;
	
	public boolean validateCountry(String countryCode) throws ApplicationException{
		/*boolean countryValid = service.validateCountry(countryCode);
		return countryValid;*/
		
		Country country = (Country) genericService.findEntity(new Country(), countryCode);
		if(country == null)
			return false;
		else
			return true;
	}
	
	/**
	 * check if a string is empty
	 * @params
	 * @return
	 */
	public static boolean isEmpty(String s){
		if(s!=null && !s.isEmpty()){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean checkContainSpecialChar(String inp){
		
		Pattern special = Pattern.compile ("[(+?%#&~`@^*(){}\\[\\]|:;',)]");
		Matcher hasSpecial = special.matcher(inp);
		
		return hasSpecial.find();
	}
	
	public boolean isAlpha(String name) {
	    char[] chars = name.toCharArray();

	    for (char c : chars) {
	        if(!Character.isLetter(c) && c!=' ') {
	            return false;
	        }
	    }

	    return true;
	}
}

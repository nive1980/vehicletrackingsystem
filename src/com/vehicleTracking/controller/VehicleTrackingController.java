package com.vehicleTracking.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vehicleTracking.dto.BookingDto;
import com.vehicleTracking.dto.DistanceDto;
import com.vehicleTracking.dto.UserDto;
import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Booking;
import com.vehicleTracking.model.Distance;
import com.vehicleTracking.model.Location;
import com.vehicleTracking.model.RestResponse;
import com.vehicleTracking.model.User;
import com.vehicleTracking.model.Vehicle;
import com.vehicleTracking.model.VehicleMake;
import com.vehicleTracking.model.VehicleModel;
import com.vehicleTracking.service.BookingService;
import com.vehicleTracking.service.CommonDataService;
import com.vehicleTracking.service.GenericService;
import com.vehicleTracking.service.LocationService;
import com.vehicleTracking.service.UserService;
import com.vehicleTracking.service.VehicleService;
import com.vehicleTracking.util.EmailUtil;


/**
 * REST layer
 *
 */
@RestController
public class VehicleTrackingController {

	@Autowired
	private UserService userService;


	@Autowired
	private BookingService bookingService;


	@Autowired
	private LocationService locationService;
	@Autowired
	private VehicleService vehicleService;

	
	@Autowired
	private VehicleTrackingControllerHelper gtnControllerHelper;

	@Autowired
	private CommonDataService commonService;

	@Autowired
	private GenericService genericService;
	
		
	@Autowired
	private EmailUtil emailUtil;

	private static final Logger logger = LoggerFactory.getLogger(VehicleTrackingController.class);

	/**
	 * @param id
	 * @return Returns the person with the given id.
	 * @throws ApplicationException
	 */
	@RequestMapping("/signup")
	public ResponseEntity<RestResponse>  signup(@RequestBody UserDto userSignUp) throws ApplicationException {
			HashMap<String, Object> responseData = new HashMap<String, Object>();
			try {
				List<String> validationResult = gtnControllerHelper.validateUserSignUp(userSignUp);
				System.out.println("validationResult :: " + validationResult);
				if(!gtnControllerHelper.isEmpty(validationResult)){
					RestResponse error = new RestResponse(false,"Server side validation fail",validationResult);
					ResponseEntity<RestResponse> result = new ResponseEntity<RestResponse>(error, HttpStatus.EXPECTATION_FAILED);
					return result;
				}

				if (userService.getUserByEmail(userSignUp.getEmail()) != null) {
					responseData.put("Status", "Fail");
					responseData.put("Message", "User with this Id already Exists");
					logger.info("User with Id {} already Exists." , userSignUp.getEmail());
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					RestResponse success = new RestResponse(true, "Successful", responseData);
					ResponseEntity<RestResponse> result = new ResponseEntity<>(success, headers, HttpStatus.OK);
					return result;
				}
			} catch (ApplicationException e) {
				// Do nothing as user needs to be created when user is not found
				// in the database
			}
			User user = gtnControllerHelper.parseInput(userSignUp);
			user.setUserType("P");
			user.setCreatedOn(new Date());
			user.setStatus("A");
			user.setSbu(generateSbu());
			userService.saveUser(user);
			logger.info("User created for id {} " , userSignUp.getEmail());
			responseData.put("Status", "Success");
			responseData.put("Username", userSignUp.getEmail());
			responseData.put("LoggedInUser", user);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			RestResponse success = new RestResponse(true, "Successful", responseData);
			ResponseEntity<RestResponse> result = new ResponseEntity<>(success, headers, HttpStatus.OK);
			return result;
	}

	private String generateSbu() throws ApplicationException{
		String sbu ;
		do
			sbu = RandomStringUtils.randomAlphanumeric(4);
		while(userService.checkSBUExist(sbu));

		return sbu;

	}
	@RequestMapping(value = "/modifyUserDetails", method = RequestMethod.POST)
	public Map<String, Object> modifyUserDetails(@RequestParam(value = "LoggedUser", required = false) String userInfo,
			@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request)
			throws ApplicationException {
		logger.info("In modifyUserDetails@@@@@@@@@ user" , userInfo);
		logger.info("In modifyUserDetails@@@@@@@@@ file" , file);
		HashMap<String, Object> response = new HashMap<String, Object>();

		try {
			ObjectMapper mapper = new ObjectMapper();
			UserDto userDto = mapper.readValue(userInfo, UserDto.class);
			com.vehicleTracking.security.SecurityUserDetail userDetail = (com.vehicleTracking.security.SecurityUserDetail) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();
			User userToBeModified = userDetail.getUser();
			// System.out.println("userToBeModified
			// "+request.getParameter("firstName"));
			userToBeModified.setFirstName(userDto.getFirstName());
			userToBeModified.setLastName(userDto.getLastName());
			userToBeModified.setTitle(userDto.getTitle());
			userToBeModified.setPhone(userDto.getPhone());
			userToBeModified.setCompanyName(userDto.getCompany());
			userToBeModified.setEmailId(userDto.getEmail());
			userToBeModified.setModifiedOn(new Date());
			String rootDirectory = "D:\\files\\";
			if (file != null) {
				// Blob blob = new
				// SerialBlob(StreamUtils.copyToByteArray(file.getInputStream()));
				// Blob blob = Hibernate.createBlob(file.getInputStream());
				// byte[] fileBytes = new byte[(int) file.length()];
				// file.getInputStream().read(fileBytes);
				userToBeModified.setImage(file.getBytes());
				// file.transferTo(new File(rootDirectory +
				// file.getOriginalFilename()));

			}
			userService.updateUser(userToBeModified);
			response.put("LoggedInUser", (userToBeModified));

		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		}
		return response;

	}

	@RequestMapping("/loginUser")
	public Map<String, Object> login(Principal user) {
		com.vehicleTracking.security.SecurityUserDetail userDetail = (com.vehicleTracking.security.SecurityUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		HashMap<String, Object> response = new HashMap<String, Object>();
		System.out.println("user is "+userDetail.getUser().getFirstName());
		System.out.println("user is "+userDetail.getUser().getId());

		response.put("LoggedInUser", (userDetail.getUser()));
		return response;
	}


	@RequestMapping("/saveBooking")
	public ResponseEntity<RestResponse> saveBooking(@RequestBody BookingDto bookingDto) throws ApplicationException {
		System.out.println("bookingDto "+bookingDto.getDropoffDate());
		System.out.println("bookingDto "+bookingDto.getDropoffLocation()+" cd "+bookingDto.getPickupDate()+"cd "+bookingDto.getPickupLocation()+"d "+bookingDto.getUserId()+bookingDto.getVehicleMake()+bookingDto.getVehicleModel());
		List<String> validationResult = gtnControllerHelper.validateBooking(bookingDto);
		System.out.println("validationResult :: " + validationResult);
		if(!gtnControllerHelper.isEmpty(validationResult)){
			RestResponse error = new RestResponse(false,"Server side validation fail",validationResult);
			ResponseEntity<RestResponse> result = new ResponseEntity<RestResponse>(error, HttpStatus.EXPECTATION_FAILED);
			System.out.println("@@@@@@@@@@@@@");
			return result;
		}

		User loggedInUser = userService.getUserByEmail(bookingDto.getUserId());
		List<Vehicle> vehicles = vehicleService.getVehicle(bookingDto,commonService);
		System.out.println("vehicles "+vehicles);
		boolean isVehicleAlreadyBookedForPeriod =false;
		if(vehicles==null || vehicles.size()==0)
			throw new ApplicationException("Vehicles are not available fpr this search criteria",new Exception("Vehicles are not available fpr this search criteria"));
		
			for(Vehicle v:vehicles) {
			isVehicleAlreadyBookedForPeriod=bookingService.isVehicleAlreadyBookedForPeriod(v,bookingDto.getPickupDate());
			System.out.println("isVehicleAlreadyBookedForPeriod "+isVehicleAlreadyBookedForPeriod);
			if(!isVehicleAlreadyBookedForPeriod) {
				Booking newBooking= new Booking();

				gtnControllerHelper.populateBooking(bookingDto, newBooking, loggedInUser,locationService,v);
				bookingService.saveBooking(newBooking);
				logger.info("Case added by {} and Ticke Id : {} ",loggedInUser.getEmailId(),newBooking.getBookingId());
				
				
				String msg = "Booking generated successfully <BR /><BR />";
				msg += "<b> booking id "+newBooking.getBookingId()+" gernrated </b>";
				//send email
				//emailUtil.setMailSender();
				try {
				emailUtil.sendMailHtml("niveditashenoy@gmail.com", bookingDto.getUserId(), "Booking generated successfully ", msg);	
				}
				catch(Exception e) {
					throw new ApplicationException(e.getMessage(), e);
				}
				break;
			}
			if(isVehicleAlreadyBookedForPeriod)
				throw new ApplicationException("Vehicle is already booked for the said time and hence cant be booked", new Exception("Vehicle is already booked for the said time and hence cant be booked"));
		
		}
		if(isVehicleAlreadyBookedForPeriod)
			throw new ApplicationException("Vehicle is already booked for the said time and hence cant be booked", new Exception("Vehicle is already booked for the said time and hence cant be booked"));
		List<BookingDto> cases = getAllBookingsByUser(loggedInUser);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestResponse success = new RestResponse(true, "Successful", cases);
		ResponseEntity<RestResponse> result = new ResponseEntity<>(success, headers, HttpStatus.OK);
	
		return result;
	}

	@RequestMapping("/getBookingsToDisplay")
	public ResponseEntity<RestResponse> getBookingsToDisplay(@RequestBody BookingDto caseDto) throws ApplicationException {
		User loggedInUser = userService.getUserByEmail(caseDto.getUserId());
		List<BookingDto> cases = getAllBookingsByUser(loggedInUser);
		logger.info("No of cases to Display : {}" ,cases.size());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestResponse success = new RestResponse(true, "Successful", cases);
		ResponseEntity<RestResponse> result = new ResponseEntity<>(success, headers, HttpStatus.OK);
		return result;
	}

	private List<BookingDto> getAllBookingsByUser(User user) throws ApplicationException {
		List<BookingDto> bookingDtos = new ArrayList<BookingDto>();
		Collection<Booking> bookings = null;
		bookings = bookingService.getBookingsByUser(user.getId());
		for (Booking bookingModel : bookings) {
			if(bookingModel.getVehicleId()==0)
				continue;
			BookingDto bookingDtoItem = gtnControllerHelper.populateBookingDto(bookingModel,locationService,vehicleService);
			bookingDtos.add(bookingDtoItem);
		}
		return bookingDtos;
	}
	@RequestMapping("/fetchMakeToModelMapping")
	public ResponseEntity<RestResponse> fetchMakeToModelMapping() throws ApplicationException {
				
			Map<String, List> returnData = new HashMap<String, List>();
			List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
			Collection<VehicleMake> makes = commonService.getVehicleMakes();
			Collection<Location> locations = commonService.getLocations();
			Collection<Distance> distances = commonService.getDistances();
			List<DistanceDto> distancedtos = new ArrayList<DistanceDto>();
			
			List<String> locationNames= new ArrayList<String>();
			for(VehicleMake make:makes) {
				String makeName=make.getName();
				Map<String, Object> makeMap = new HashMap<String, Object>();
				makeMap.put("make", makeName);
			
				List<String> modelNames= new ArrayList<String>();
				
				Collection<VehicleModel> models = commonService.getVehicleModels(make.getId());
				
				for(VehicleModel model:models) {
					modelNames.add(model.getName());
					
					
				}
				makeMap.put("model", modelNames);
				result.add(makeMap);
				
			
			}
			Map<String, Object> makeMap1 = new HashMap<String, Object>();
			for(Location location:locations) {
				locationNames.add(location.getLocationId()+"~"+location.getLocationName());
				
				
			}	
			for(Distance distance:distances) {
				DistanceDto disdto = new DistanceDto();
				disdto.setFrom(locationService.getLocation(distance.getLocation1Id()).getLocationName());
				disdto.setTo(locationService.getLocation(distance.getLocation2Id()).getLocationName());
				disdto.setTime(""+distance.getTime());
				disdto.setDistance(""+distance.getDistance());
				disdto.setDistanceId(""+distance.getDistanceId());
				distancedtos.add(disdto);
			}
			makeMap1.put("locationFrom", locationNames);
			makeMap1.put("locationTo", locationNames);
			makeMap1.put("distances", distancedtos);
			result.add(makeMap1);
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			RestResponse success = new RestResponse(true, "Successful", result);
			ResponseEntity<RestResponse> result1 = new ResponseEntity<>(success, headers, HttpStatus.OK);
			return result1;

	}

	
	
}

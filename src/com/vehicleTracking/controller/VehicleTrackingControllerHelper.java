package com.vehicleTracking.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vehicleTracking.dto.BookingDto;
import com.vehicleTracking.dto.UserDto;
import com.vehicleTracking.exception.ApplicationException;
import com.vehicleTracking.model.Booking;
import com.vehicleTracking.model.User;
import com.vehicleTracking.model.Vehicle;
import com.vehicleTracking.service.LocationService;
import com.vehicleTracking.service.VehicleService;
import com.vehicleTracking.util.ValidationUtil;


@Repository("gtnControllerHelper")
public class VehicleTrackingControllerHelper {

	public User parseInput(UserDto dto) {
		User user = new User();
		user.setFirstName(dto.getFirstName());
		user.setLastName(dto.getLastName());
		user.setEmailId(dto.getEmail());
		user.setPhone(dto.getPhone());
		user.setPassword(dto.getPassword());
		return user;
	}
	
	public UserDto prepareUserDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setEmail(user.getEmailId());
		userDto.setCompany(user.getCompanyName());
		userDto.setId(""+user.getId());

		userDto.setPhone(user.getPhone());
		return userDto;
	}
	public BookingDto populateBookingDto(Booking bookingModel, LocationService locationService, VehicleService vehicleService) throws ApplicationException{
		BookingDto bookingDto = new BookingDto();
		String FORMATER = "MM/dd/yy HH:mm aa";
	    DateFormat format = new SimpleDateFormat(FORMATER);
	  //  format.parse(source)
		bookingDto.setDropoffDate(""+bookingModel.getDropoffDate());
		bookingDto.setDropoffLocation(""+locationService.getLocation(bookingModel.getDropoffLocationId()).getLocationName());
		bookingDto.setPickupDate(""+bookingModel.getPickUpDate());
		bookingDto.setPickupLocation(""+locationService.getLocation(bookingModel.getPickupLocationId()).getLocationName());
		bookingDto.setUserId(""+bookingModel.getUserId());
		System.out.println("getVehicleId "+bookingModel.getVehicleId());
		if(vehicleService.getVehicleMake(bookingModel.getVehicleId())!=null)
			bookingDto.setVehicleMake(vehicleService.getVehicleMake(bookingModel.getVehicleId()));
		if(vehicleService.getVehicleModel(bookingModel.getVehicleId())!=null)
			bookingDto.setVehicleModel(vehicleService.getVehicleModel(bookingModel.getVehicleId()));
		return bookingDto;		
	}

		

	public Booking populateBooking(BookingDto bookingDto, Booking bookingModel,User user,LocationService locationService, Vehicle v) throws ApplicationException {
		if(bookingDto.getDropoffDate()!=null && !bookingDto.getDropoffDate().equals("")){
			String FORMATER = "MM/dd/yy HH:mm aa";

		    DateFormat format = new SimpleDateFormat(FORMATER);
		    try {
				Date dt=format.parse(bookingDto.getDropoffDate());
				bookingModel.setDropoffDate(dt);
				
		    } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				String FORMATER1 = "MM/dd/yy HH:mm aa";

			    DateFormat format1 = new SimpleDateFormat(FORMATER);

				try {
					System.out.println("format1 "+format1.toString());
					System.out.println("cds "+bookingDto.getDropoffDate());
					Date dt1= new Date(Long.parseLong(bookingDto.getDropoffDate()));
				    
					String newDate=format1.format(dt1);
					System.out.println("newDate "+newDate);
					bookingModel.setDropoffDate(new Date(newDate));
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}


			}
		}
		//bookingModel.setDropoffLocationIdTo(new Long(bookingDto.getDropoffLocation()));
		if(bookingDto.getPickupDate()!=null && !bookingDto.getPickupDate().equals("")){
			String FORMATER = "MM/dd/yy HH:mm aa";
			System.out.println("bookingDto.getPickupDate() "+bookingDto.getPickupDate());
		    DateFormat format = new SimpleDateFormat(FORMATER);
		    try {
			Date dt=format.parse(bookingDto.getPickupDate());
			System.out.println("dt "+dt);
			bookingModel.setPickUpDate(dt);
		    } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		}
		bookingModel.setPickupLocationId(new Long(bookingDto.getPickupLocation().substring(0,1)));
		bookingModel.setUserId(user.getId());
		bookingModel.setDropoffLocationId(new Long(bookingDto.getDropoffLocation().substring(0,1)));
		bookingModel.setVehicleId(v.getId());

		
		return bookingModel;
	}


/*	public CaseLog populateCaseLog(CaseLogDto caseLogDto, long messageBy) throws ApplicationException {
		try {
			CaseLog caseLog = new CaseLog();
			caseLog.setTicketId(caseLogDto.getTicketId());
			caseLog.setMessage(caseLogDto.getMessage());
			caseLog.setMessageBy(messageBy);
			if (caseLogDto.getMessageOn() == null)
				caseLog.setMessageOn(new Date());
			else {
				SimpleDateFormat sdfr = new SimpleDateFormat("MM/dd/yyyy");
				caseLog.setMessageOn(sdfr.parse(caseLogDto.getMessageOn()));
			}
			return caseLog;
		} catch (ParseException e) {
			throw new ApplicationException("System Error", e);
		}
	}*/
	
	public boolean isEmpty(Object object) {

		if (object == null)
			return true;

		if(object instanceof Collection)
			return ((Collection)object).isEmpty();
		
		if (object instanceof String)
			return (((String) object).trim().length() > 0) ? false : true;

		return false;
	}
			
	public List<String> validateUserSignUp(UserDto userDto){
		List<String> validationError = new ArrayList<String>();
		if(!ValidationUtil.ValidateMandatoryString(userDto.getFirstName(), 50) )
			validationError.add("Enter a valid First Name.");
		if(!ValidationUtil.ValidateMandatoryString(userDto.getLastName(), 50) )
			validationError.add("Enter a valid Last Name.");
		if(!ValidationUtil.validateEmail(userDto.getEmail(), 50) )
			validationError.add("Enter a valid Email Id.");
		if(!ValidationUtil.ValidateMandatoryString(userDto.getPassword(), 50) )
			validationError.add("Enter a valid Password.");
		if(!ValidationUtil.ValidateMandatoryNumericString(userDto.getPhone(), 20) )
			validationError.add("Enter a valid Phone.");
		return validationError;
	}
	
	public List<String> validateAdditionalUserSignUp(UserDto userDto){
		List<String> validationError = new ArrayList<String>();
		if(!ValidationUtil.ValidateMandatoryString(userDto.getFirstName(), 50) )
			validationError.add("Enter a valid First Name.");
		if(!ValidationUtil.ValidateMandatoryString(userDto.getLastName(), 50) )
			validationError.add("Enter a valid Last Name.");
		if(!ValidationUtil.validateEmail(userDto.getEmail(), 50) )
			validationError.add("Enter a valid Email Id.");
		return validationError;
	}
	public List<String> validateBooking(BookingDto bookingDto){
		List<String> validationError = new ArrayList<String>();
		if(!ValidationUtil.ValidateMandatoryString(bookingDto.getPickupLocation(), 1000) )
			validationError.add("Enter a valid Location.");
		/*if((caseDto.getCategory()<= 0) )
			validationError.add("Enter a Category.");*/		
		if(!ValidationUtil.ValidateMandatoryString(bookingDto.getDropoffLocation(), 1000) )
			validationError.add("Enter a valid Drop Off Location.");
	
		if(!ValidationUtil.ValidateMandatoryString(bookingDto.getPickupDate(), 1000) )
			validationError.add("Enter a valid Pick up Date");
		if(!ValidationUtil.ValidateMandatoryString(bookingDto.getVehicleMake(), 1000) )
			validationError.add("Enter a valid Make");
		if(!ValidationUtil.ValidateMandatoryString(bookingDto.getVehicleModel(), 1000) )
			validationError.add("Enter a valid Model");
		return validationError;
	}
	
	public List<String> validateAdditionalUser(UserDto userDto){
		List<String> validationError = new ArrayList<String>();
		if(!ValidationUtil.ValidateMandatoryString(userDto.getFirstName(), 50) )
			validationError.add("Enter a valid First Name.");
		if(!ValidationUtil.ValidateMandatoryString(userDto.getLastName(), 50) )
			validationError.add("Enter a valid Last Name.");
		if(!ValidationUtil.validateEmail(userDto.getEmail(), 50) )
			validationError.add("Enter a valid Email Id.");
		return validationError;
	}
	
}

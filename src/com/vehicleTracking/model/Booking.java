package com.vehicleTracking.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BOOKING")
public class Booking implements Model {
	
	private long bookingId;
	private long userId;
	private long pickupLocationId;
	private long dropoffLocationId;
	private Date pickUpDate;
	@Column(name = "DROP_OFF_DATE")
	public Date getDropoffDate() {
		return dropoffDate;
	}
	public void setDropoffDate(Date dropoffDate) {
		this.dropoffDate = dropoffDate;
	}
	public void setDropoffLocationId(long dropoffLocationId) {
		this.dropoffLocationId = dropoffLocationId;
	}
	private long vehicleId;
	private Date dropoffDate;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getBookingId() {
		return bookingId;
	}
	public void setBookingId(long bookingId) {
		this.bookingId = bookingId;
	}
	@Column(name = "USER_ID")
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	@Column(name = "PICK_UP_LOCATION")
	public long getPickupLocationId() {
		return pickupLocationId;
	}
	public void setPickupLocationId(long pickupLocationId) {
		this.pickupLocationId = pickupLocationId;
	}
	@Column(name = "VEHICLE")
	public long getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}
	@Column(name = "pick_up_date")
	public Date getPickUpDate() {
		return pickUpDate;
	}
	public void setPickUpDate(Date pickUpDate) {
		this.pickUpDate = pickUpDate;
	}
	@Column(name = "DROP_OFF_LOCATION")
	public long getDropoffLocationId() {
		return dropoffLocationId;
	}
	public void setDropoffLocationIdTo(long dropoffLocationId) {
		this.dropoffLocationId = dropoffLocationId;
	}
}

package com.vehicleTracking.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DISTANCE")
public class Distance implements Model {
	
	private long distanceId;
	private long location1Id;
	private long location2Id;
	private float distance;
	private float time;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getDistanceId() {
		return distanceId;
	}
	public void setDistanceId(long distanceId) {
		this.distanceId = distanceId;
	}
	
	@Column(name = "LOCATION1")
	public long getLocation1Id() {
		return location1Id;
	}
	public void setLocation1Id(long location1Id) {
		this.location1Id = location1Id;
	}
	@Column(name = "LOCATION2")
	public long getLocation2Id() {
		return location2Id;
	}
	public void setLocation2Id(long location2Id) {
		this.location2Id = location2Id;
	}
	@Column(name = "DISTANCE")
	public float getDistance() {
		return distance;
	}
	public void setDistance(float distance) {
		this.distance = distance;
	}
	@Column(name = "TIME")
	public float getTime() {
		return time;
	}
	public void setTime(float time) {
		this.time = time;
	}

	
		}
